Project information
-------------------

 This repository contains my files (my code snippets and my synopses about the main book's topics) 
 for the book **"Professional JavaScript for Web Developers"** *3-d edition* by **Nicholas C. Zakas**.

 
 [here is the link to amazon](http://www.amazon.com/Professional-JavaScript-Developers-Nicholas-Zakas/dp/1118026691). 


Installation
------------

 Simply copy (clone the repository) and see and read it.

 
Basic usage
-----------
 
 `$ git clone https://gitlab.com/book_synopses_and_code_samples/professional_js_for_web_developers_3_ed_n_c_zakas.git`

 
License
-------

 This project is offered under the MIT license.


Contributing
------------

 This project doesn't need to be contributed.

 But just in case you can always connect with me.