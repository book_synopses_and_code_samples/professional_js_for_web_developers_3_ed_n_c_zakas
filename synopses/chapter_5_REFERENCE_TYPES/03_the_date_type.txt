 #############
 the date type
 #############
 
 the date type
 =============

 The ECMAScript "Date" type is based on a early version of java.util.Date 
 from Java. 
 As such, the "Date" type store dates as the number of milliseconds
 that have passed since midnight on January 1, 1970 UTC
 (Universal Time Code).

 When the Date constructor is used without any arguments, 
 the created object is assigned the current date and time. 

 To create a date based on another date or time, 
 you must pass in the millisecond representation of the date
 (the number of milliseconds after midnight, January 1, 1970 UTC). 
 To aid in this process, ECMAScript provides two methods: 
 - Date.parse() 
 - Date.UTC()

 The Date.parse() method accepts a string argument representing a date.

 If the string passed into Date.parse() doesn�t represent a date, 
 then it returns NaN. The Date constructor will call Date.parse()
 behind the scenes if a string is passed in directly.

 The Date.UTC() method also returns the millisecond representation of a date
 but constructs that value using different information than Date.parse(). 
 The arguments for Date.UTC() are 
 - the year, 
 - the zero-based month (January is 0, February is 1, and so on), 
 - the day of the month (1 through 31),
 - the hours (0 through 23), minutes, seconds, and milliseconds of the time. 
 Of these arguments, only the first two (year and month) are required.
 If the day of the month isn�t supplied, it�s assumed to be 1, 
 while all other omitted arguments are assumed to be 0.

 ECMAScript 5 adds Date.now(), which returns the millisecond representation 
 of the date and time at which the method is executed.

 inherited methods
 =================

 As with the other reference types, the Date type overrides
 toLocaleString(), toString(), and valueOf(), 
 though unlike the previous types, each method returns something different.

 The valueOf() method for the Date type doesn�t return a string at all, 
 because it is overridden to return the milliseconds representation 
 of the date so that operators (such as less-than and greater-than)
 will work appropriately for date values. 