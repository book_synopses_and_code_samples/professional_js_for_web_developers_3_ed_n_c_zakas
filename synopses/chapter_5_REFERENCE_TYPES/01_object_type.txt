 ###########
 object type
 ###########

 object type
 ===========

 Objects are considered to be instances of a particular reference type.
 New objects are created by using the new operator followed by a contructor.
 A constructor is simply a function whose purpose is to create a new object.
 --------------------------------------------------------------------
 | var person = new Object();                                       |
 --------------------------------------------------------------------
 
 There are two ways to explicitly create an instance of Object. 
 - The first is to use the "new" operator with the "Object" constructor 
 - The other way is to use object literal notation.

 It is possible to access properties via dot notation and bracket notation.

 The main advantage of bracket notation is that it allows you to use
 variables or names, that contains nonalphanumeric characters,
 for property access